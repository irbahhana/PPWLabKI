# Lab 6: Introduction to Javascript and JQuery_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Objectives

What you should understand after finishing this tutorial 

- Understanding Javascript
- Introduction to Javascript function in Front-End Developer
- Basic javascript usage
- Development of javascript technology

## End Result
- Created a calculator with JavaScript
- Created a chat box with JavaScript
- Created a theme changer feature with select2

## Introduction to Javascript

#### What is JavaScript


JavaScript is a cross platform high-level multi-paradigm programming language. This multi-paradigm allows JavaScript to support Object Oriented Programming, Imperative Programming, and Functional Programming. JavaScript itself is a implementation of ECMAScript, which is the core of JavaScript programming language. Another language implemented from ECMAScript are JScript (Microsoft) and ActionScript(Adobe).

Javascript, along with HTML and CSS, combined into 3 main technology used for web development. Advantages of using Javascript for web development is basically Javascript can manipulate web page dynamically and engaging more interaction to the user. Therefore, almost every modern website are using JavaScript in their web page to provide a good user experience. Another example we can do with JavaScript are displaying time information, identify type of browser used by the user, perform form validation or data validation, create [cookies](https://en.wikipedia.org/wiki/HTTP_cookie), dynamic CSS and styling for an element, etc. During web development process, JavaScript code are used on the client-side, but some kind of JavaScript code are used on the server-side, example such as node.js; The terms Client-side refers to JavaScript code that are executed or run at user’s browser, not on the web server, so the complexity of a JavaScript code won’t affect much on the performance of the web servers. On the other hand, it increases the amount of memory used by the browser.

In this course, we’ll focus on JavaScript code on the client-side.

#### How are JavaScript executed by the browser ?

Take look at the diagram below.

![javascript-works](https://preview.ibb.co/e258TG/Screenshot_from_2017_10_31_14_29_13.png)

After the browser have downloaded all of the HTML pages needed, it will look at the script tag contained in the HTML page, and checks whether there’s a embedded JavaScript code, or refers to an external JavaScript file. If it’s an external file, then the browser will download it first.


__Writing a JavaScript code__

JavaScript can be written by __Embedded JavaScript__ or __External JavaScript__. JavaScript code can be defined or be written by embedding to a file or seperated within another file. If written externally, it will use the .js file extension. Below is the example of a JavaScript code.


JavaScript can be put on the head or the body of a HTML page. A JavaScript code __HAVE__ to be put between a ```<script>``` and a ```</script>``` tag. You can put more than one script tag on a HTML page.

__Embedded JavaScript on HTML__

index.html
```html
<script type="text/JavaScript">
    alert("Hello World!");
</script>
```

__External JavaScript on HTML__

index.html
```html
<script type="text/JavaScript" src="js/script.js"></script>
```
js/script.js
```javascript
alert("Hello World!")
```
```<script>``` tag are not needed in external JavaScript file.
Separating JavaScript file externally gives you some benefits such as code reusability, no mixing up JavaScript code and HTML therefore giving more focus, increases loading speed (.js File will be cached by the browser, so it won’t be requested after the first one).

#### JavaScript execution

After all the JavaScript code are successfully downloaded, the browser will automatically execute all the code. If the code is NOT an event triggered code, then it will be directly executed. If it is, it will be executed the moment a specific event is triggered.


```javascript
// directly executed
alert("Hello World");

// directly executed
var obj = document.getElementById("object");
// directly executed, add event handler onclick for element objec
obj.onclick = function() {
    // only executed if element 'object' clicked
    alert("You just click the object!");
};
```

#### JavaScript Syntax

__Variable__

Declaring variable in JavaScript are quiet easy. For example

```javascript
    var example = 0;     // var example is a number
    var example = "example";// var example is a string
    var example = true;  // var example is a boolean
```

Javascript could accommodate many data type, start from string, integer to Object though.
Contrast to Java that data type is distinguished  with head variable(int, int = 9), JavaScript has special loosely type characteristic or a dynamic language. We don't need to write data type on head variable, and JavaScript will be automatically read your data type based on existing standard. Just like the example above. For further details you can click this link.

There are some rules in selection of identifiers or variable name in JavaScript. First character must be an alphabet, underscore(_), or dollar character ($). Moreover JavaScript identifiers are case sensitive.

__Arithmetic__

Javascript can also perform arithmetic operation, for example down below is a simple calculator with HTML DOM (DOM will be explained later).

index.html code

```javascript
...
<form>
    <h3>Kalkulator Penambahan </h3>
    Angka Pertama: <input type="text" id="number1" />
    Angka Kedua: <input type="text" id="number2" />
    <input type="button" name="submit" value="Submit" onclick="tambahkan();" />
    <p id ="hasil"></p>
</form>
...
```
javascript.js code

```javascript
function tambahkan() {
    var x = parseInt(document.getElementById("number1").value);
    var y = parseInt(document.getElementById("number2").value);
    var z = x+y;
    document.getElementById("hasil").innerHTML = z;
}
```

__String Concatenation__

In JavaScript, we also can connect string with another string just like in Java.

```javascript
var str1 = "PPW"+" "+"Asik";
var str2 = "PPW";
var str3 = "Asik";
var str4 = str2+" "+str3;
```

#### Javascript Scope

__Local Variable__

Variable declared in a function can only be accessed by code inside that function. There variables are local.

```javascript
// code outside iniFungsi() cannot access namaMatkul variable
function iniFungsi() {
    var namaMatkul = "PPW";
    // code inside this function can access namaMatkul variable
}
```

__Global Variable__

Variable declared outside function are global and can be accessed by another code inside JavaScipt file.

```javascript
var namaMatkul = "PPW";
function iniFungsi() {
    // code inside this function can access namaMatkul variable
}
```

__Auto Global Variable__

Value assigned on a variable that not yet declared automatically become global variable even though that variable located in a function.

```javascript
iniFungsi(); // function iniFungsi() have to be called first
console.log(namaMatkul); // print "PPW" on JavaScript console
function iniFungsi() {
    namaMatkul = "PPW";
}
```

__Accessing Global Variable from HTML__

You can access global variable inside the JavaScript file on HTML file that download the JavaScript file.

```html
...
<input type="text"
onclick="this.value=namaMatkul"/>
...
```

```javascript
...
var namaMatkul = "PPW";
...
```

#### Function and Event

Functions are groups of codes that can be called anywhere on section of code (similar to java method). This reduces redundancy to reduces the same code repeatedly. Other than that, JavaScript function is so useful to facilitate dynamic calling elements. Function can be called by another function and also be called because of an event (will be explained below). For example, this code inside index.html.

```html
...
<input type="button" value="magicButton" id="magicButton" onclick="hore();" />
...
```

Javascript.js code

```javascript
...
function hore(){
    alert("Hati­hati tugas 1 PPW berbahaya!");
}
...
```

So if magicButton is pressed, then the onclick function will run hore function on javascript.js, and then the alert will pop up on top of the screen.

Onclick code is one of javascript property called event. Event is javascript ability to make a dynamic website. Onclick used as a toggle what JavaScript will do if the element is pressed. Furthermore, usually event given a useful function as command for JavaScript. Another event example is onchange, onmouseover, onmouseout , etc. (Another event examples)(https://www.tutorialspoint.com/javascript/javascript_events.html)


#### Javasciprt HTML & CSS DOM

__HTML DOM__

HTML DOM (Document Object Model) is standard fto change, get, delete HTML Elements. HTML DOM can be accessed through JavaScript or another programming language. For more details you can read it on (https://www.w3schools.com/js/js_htmldom.asp)

Code below is the example of DOM Implementation :

```html
...
    <div>
        <p onclick="myFunction()" id="demo" >Ini Percobaan HTML DOM</p>
    </div>
...
```

```javascript
...
    function myFunction() {
document.getElementById("demo").innerHTML = "YOU CLICKED ME!";
    }
...
```

__CSS DOM__

Same with HTML DOM, DOM CSS can change CSS dyncamically through JavaScript. For more information : (https://www.w3schools.com/js/js_htmldom_css.asp) 

For example :

index.html

```html
...
<p id="textBiru" onclick="gantiWarna()">Click me v2</p>
...
```

javascript.js
```javascript
...
function gantiWarna(){
    document.getElementById("textBiru").style.color="blue";
}
...
```

## Web Storage

With local storage, web application can save data locally in user browser. Before HTML5, application data must be save inside cookies, including on every server request. Local storage is more safety and amount of data will be save locally without effecting website performance. Unlike cookies, storage limits is bigger (at least 5MB) and the information will never be transferred to server. Local storage use per domain and protocol (All pages, from one resource can save and access the same data).

There are 2 ways to save data with web storage :
    - window.localStorage - save data without the expiry date
    - window.sessionStorage - save data for one session (data will be lost when the tab is closed)

__localStorage Object__

LocalStorage object save data without the expiry date, means data will not be deleted when the browser is closed, and will be available on the next days, weeks or years. 

For example:

index.html
```html
...
<p><button onclick="clickCounter()" type="button">Click me!</button></p>
<div id="result"></div>
<p>Click the button to see the counter increase.</p>
<p>Close the browser tab (or window), and try again, and the counter will continue to count (is not reset).</p>
...
```

```javascript
...
function clickCounter() {
    if(typeof(Storage) !== "undefined") {
        if (localStorage.clickcount) {
            localStorage.clickcount = Number(localStorage.clickcount)+1;
        } else {
            localStorage.clickcount = 1;
        }
        document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s).";
    } else {
        document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
    }
}
...
```

If we run the program, when the button is clicked then click count will run (increasing).
Whenever the browser is closed, and if we open it again, then we can see that click count will be continue from latest count.

__sessionStorage Object__

Sama as above, but using sessionStorage means whenever the browser is closed if we opened the page again, click count will be 0 again.

For further information about [HTML5 WebStorage](http://www.w3im.com/id/html/html5_webstorage.html)


## Pengenalan jQuery

There is another JavaScript library which quite famous, called jQuery. Just like it slogan The Write Less, Do More, jQuery make JavaScript code very simple. JQuery is made by John Resign on 2006. Several javascript code such as document traversing, event handling, animating and AJAX is supported by jQuery with an easy access. JQuery installation provided in 2 ways, first, we download jQuery from jQuery.com or second, access it through CDN. In this tutorial we will use the second way to access jQuery (Using CDN).

In this tutorial we will use external JavaScript and jQuery, so make __lab_6.js__ file from your favourite text editor on lab_6/static/js folder.

```javascript
$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
});
```

## Static files di Django

On Django Framework, there are files that called static files. `static files` are HTML support files on a website. Examples of `static files` are CSS, JavaScript and pictures. Settings for `static files` is inside `settings.py`

```python
    ...

    # Static files (CSS, JavaScript, Images)
    # httpsdocs.djangoproject.comen1.9howtostatic-files
    STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
    STATIC_URL = 'static'

    ...
```

On `settings.py` there is  `STATIC_ROOT` which determine _absolute path_ to `static files` directory when running `collectstatic command to project and there is `STATIC_URL` which is url that can be accessed by public to get the `static files`

 `collectstatic` command used for collecting `static files` from all `app` making it easier to access to all `app`

Further information about static files :
[static files](httpsdocs.djangoproject.comen1.11howtostatic-files)

#### Create a simple ChatBox
1. Make `lab_6.css` inside`lab_6/static/css` (If the folder is not available, create the folder)
    ```css
        *{
          padding: 0px;
          margin: 0px;
          font-family: 'Fira Code';
        }
        body{
          height: 100%;
          background: #95a5a6;
        }
        .chat-box{
          position: absolute;
          right: 20px;
          bottom: 0px;
          background: white;
          width: 300px;
          border-radius: 5px 5px 0px 0px;
        }
        .chat-head{
          width: inherit;
          height: 100%;
          background: #2c3e50;
          border-radius: 5px 5px 0px 0px;
        }
        .chat-head h2{
          color: white;
          padding: 8px;
          display: inline-block;
        }
        .chat-head img{
          cursor: pointer;
          float: right;
          width: 25px;
          margin: 10px;
        }
        .chat-body{
          height: 355px;
          width: inherit;
          overflow: auto;
          margin-bottom: 45px;
        }
        .chat-text{
          position: fixed;
          bottom: 0px;
          height: 45px;
          width: inherit;
        }
        .chat-text textarea{
          width: inherit;
          height: inherit;
          box-sizing: border-box;
          border: 1px solid #bdc3c7;
          padding: 10px;
          resize: none;
        }
        .chat-text textarea:active, .chat-text textarea:focus, .chat-text textarea:hover{
          border-color: royalblue;
        }
        .msg-send{
          background: #2ecc71;
        }
        .msg-receive{
          background: #3498db;
        }
        .msg-send, .msg-receive{
          width: 200px;
          height: 35px;
          padding: 5px 5px 5px 10px;
          margin: 10px auto;
          border-radius: 3px;
          line-height: 30px;
          position: relative;
          color: white;
        }
        .msg-receive:before{
          content: '';
          width: 0px;
          height: 0px;
          position: absolute;
          border: 15px solid;
          border-color: transparent #3498db transparent transparent;
          left: -29px;
          top: 7px;
        }
        .msg-send:after{
          content: '';
          width: 0px;
          height: 0px;
          position: absolute;
          border: 15px solid;
          border-color: transparent transparent transparent #2ecc71;
          right: -29px;
          top: 2.5px;
        }
        .msg-receive:hover, .msg-send:hover{
          opacity: .9;
        }
    ```

2. Make `base.html` inside `lab_6/templates/lab_6/layout` (If the folder is not available, create the folder)
    ```html
    {% load staticfiles %}
    {% load static %}
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="LAB 5">
        <meta name="author" content="{{author}}">
        <!-- bootstrap csss -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{% static 'css/lab_6.css' %}" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700">
        <title>
            {% block title %} Lab 6 By {{author}} {% endblock %}
        </title>
    </head>
    <body>
        <header>
            {% include "lab_6/partials/header.html" %}
        </header>
        <content>
                {% block content %}
                    <!-- content goes here -->
                {% endblock %}
        </content>
        <footer>
            <!-- TODO Block Footer dan include footer.html -->
            {% block footer %}
            {% include "lab_6/partials/footer.html" %}
            {% endblock %}
        </footer>
        <!-- Jquery n Bootstrap Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
    ```
3. To be able to access __script__ that you have made, add link to `lab_6.js` on `base.html`
    ```html
        ...
        <!-- Jquery n Bootstrap Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="{% static 'js/lab_6.js' %}"></script>
        ...
    ```

4. Make `lab_6.html` in `lab_6/templates/lab_6/`
    ```html
    {% extends "lab_6/layout/base.html" %}

    {% block content %}
        <!-- ChatBox Section -->
        <section name="Chatbox">
           <div class="wrapper">
              <div class="chat-box">
                 <div class="chat-head">
                    <h2>Chat</h2>
                    <img src="https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png" title="Expand Arrow" width="16">
                 </div>
                 <div class="chat-body">
                    <div class="msg-insert">
                    </div>
                    <div class="chat-text">
                       <textarea placeholder="Press Enter"></textarea>
                    </div>
                 </div>
              </div>
           </div>
        </section>
        {% endblock %}
    ```

5. Make `lab_6.js` inside `lab_6/static/js` (If the folder is not available, create the folder)
6. Complete code inside file __lab_6.js__ so it can be run.
7. Set `static file` so it can be deployed on heroku.
    1. Install whitenoise
        > pipenv install whitenoise
    2. On `settings.py` make sure middleware settings and storage follow this
        ```python
        MIDDLEWARE_CLASSES = (
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/
            'whitenoise.middleware.WhiteNoiseMiddleware',
            ...
        ```
        ```python
            ...
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/

            STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
        ```
    3. On build process, heroku can automatically run ` python manage.py collectstatic --noinput`. In case there is failur on build process, then run `heroku config:set DEBUG_COLLECTSTATIC=1` to find out what error are occured.
    4. If you still cannot deploy your project to heroku, you can use this link [Help Deploy Heroku](https://devcenter.heroku.com/articles/django-assets)

8. _Commit_ dan _push_.

#### Make a Calculator
1. Add this code below into  `lab_6.css` file
    ```css
    ...

    .calculator {
      margin-top: 15%;
      margin-bottom: 15%
    }

    .calculator .model {
      background: #4e4e4e;
    }
    .calculator .calcs {
      font-size: 56px;
      padding-top: 15px;
      padding-bottom: 15px;
      height: auto;
      border-radius: 0;
      cursor: text;
    }

    .calculator #title {
      color: #f9f9f9;
    }

    .calculator .btn {
      border-radius: 0;
    }

    ...
    ```
2. Add this code below into `base.html` file inside `lab_6/templates/lab_6/layout` (If the folder not yet available, make sure you have following the instruction before)
    ```html
    ...

      <!-- Calculator Section -->
      <section class="calculator">
        <div class="container">
            <div class="model col-lg-5">
                <div class="container-fluid">
                    <div class="row" id="title">
                        <h1 class="text-center">
                            CALCULATOR
                        </h1>
                    </div>
                    <div class="row" id="calcs-section">
                        <div class="row">
                            <input id="print" type="text" readonly="readonly" class="form-control text-right calcs" id="usr">
                        </div>
                    </div>
                    <!-- Adapted from Tom Howard -->
                    <div class="row" id="calc-buttons">
                        <div class="row">
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go('ac');">AC</button>
                            <!-- <button class="btn btn-lg btn-default col-xs-3" onClick="go('log');">log</button> -->
                            <!-- <button class="btn btn-lg btn-default col-xs-3" onClick="go('sin');">sin</button> -->
                            <!-- <button class="btn btn-lg btn-default col-xs-3" onClick="go('tan');">tan</button> -->
                        </div>
                        <div class="row">
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(7);">7</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(8);">8</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(9);">9</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(' * ');">*</button>
                        </div>
                        <div class="row">
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(4);">4</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(5);">5</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(6);">6</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(' - ');">-</button>
                        </div>
                        <div class="row">
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(1);">1</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(2);">2</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(3);">3</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(' + ');">+</button>
                        </div>
                        <div class="row">
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(0);">0</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go('.');">.</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go('eval');">=</button>
                            <button class="btn btn-lg btn-default col-xs-3" onClick="go(' / ');">/</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    ...
    ```
3. Add this code below into `lab_6.js` file inside `lab_6/static/js` (If the folder or the file is not yet available, make sure you have following the instruction before). Make placements adjustment so the script can run.
    ```javascript
    ...

    // Calculator
    var print = document.getElementById('print');
    var erase = false;

    var go = function(x) {
      if (x === 'ac') {
        /* implemetnasi clear all */
      } else if (x === 'eval') {
          print.value = Math.round(evil(print.value) * 10000) / 10000;
          erase = true;
      } else {
        print.value += x;
      }
    };

    function evil(fn) {
      return new Function('return ' + fn)();
    }
    // END

    ...
    ```
4. Implement `AC` feature on calculator.
5. _Commit_ dan _push_.

## Make Change Theme feature with Select2 Library
1. Import select2 library and select2 css into `lab_6.html`. You can use CDN that provided from select2.org page to import select2 library and select2 css on head section lab_6.html
    ```html
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    
    ```
1. Make an ```<select>``` element and apply button on `lab_6.html`. Add ```<select>``` element on `lab_6.html`. For example:
    ```html
    
    <select class="my-select"></select>
    ...
    <button class="apply-button">apply</button>
    
    ```
    Arrange both of the element neatly on `lab_6.html` page

1. Open lab_6.js file add this code below to initiate select2
    ```javascript
    
    $(document).ready(function() {
        $('.my-select').select2();
    });
    
    ```
The code is required to initiate select2 to element ```<select>```  which is aimed. Make sure that the class is match. 

Initiate JSON themes data and selected theme in Local Storage.
    
    `themes`
    
    ```java
    
    "[
        {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
        {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
        {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
        {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
        {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
        {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
        {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
        {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
        {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
        {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
        {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ]"
    
    ```
    
    `selectedTheme`
    
    ```
    
    {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}
    
    ```
    You can explore your self about JSON.parse() and JSON.stringify() on [JSON Parse](https://www.w3schools.com/js/js_json_parse.asp) and [JSON Stringify](https://www.w3schools.com/js/js_json_stringify.asp)

1. Load JSON from Local Storage

    Load themes from local storage as a data for select2.
    Load selectedTheme from local storage as a data for default theme. 
  Value of selectedTheme will be used for default when the page loads first.

Remember how to get value from local storage as previously described.

1. Populate data themes for select2

    Populate data select2 useoptions which defined by select2 documentation.

 For further information you can refer to this page :
[http://select2.org/data-sources/formats](https://select2.org/data-sources/formats)    
    ```javascript
    
    $('.my-select').select2({
        'data': JSON.parse(**ISI DENGAN DATA THEMES DARI LOCAL STORAGE**)
    })
    
    ```
DONT FORGET to parse JSON data from local storage first before using it.

1. Make a handler when  __Apply__  button is pressed
When apply button is pressed so chosen theme must be directly applied to the page.
    Give onClick handler for elemem button Apply.
    ```javascript
    
    $('.apply-button-class').on('click', function(){  // sesuaikan class button
        // [TODO] get value from select .my-select element
        
        // [TODO] fit the ID theme selected with the theme list
        
        // [TODO] take the selected object theme
        
        // [TODO] apply modification to entire HTML element which the color need to be changed
        
        // [TODO] save the object theme into local storage selectedTheme
    })
    
    ```
    
    If successed then background color and HTML font element selected will change color as chosen before.Moreover selectedTheme data on local storage are also change.

## Unit Testing Qunit (Additional)
(Highly recomended to implement `AC`feature on calculator)
1. Add this code below into your `lab_6/layout/base.html`  :

    ```html
    
    <link rel="stylesheet" href="https://code.jquery.com/qunit/qunit-2.4.1.css">
    <head>
    ......
    ......
    <body>
    <div id="qunit"></div>
    ......
        <script src="{% static 'js/lab_6.js' %}"></script>
        <script src="https://code.jquery.com/qunit/qunit-2.4.1.js"></script>
    .....
    
    ```

2. Reload the web page so you can see above the calculator will appear Qunit view
3. We will try to make _Unit Test_ for the Calculator we’ve made. Create `static/js/test.js`
File inside `lab_6` apps. Fill that file with this code below :


    ```javascript
        
        $( document ).ready(function() {
        var button_8 = $('button:contains("8")');
        var button_4 = $('button:contains("4")');
    
        var button_add = $('button:contains("+")');
        var button_sub = $('button:contains("-")');
        var button_mul = $('button:contains("*")');
        var button_div = $('button:contains("/")');
    
        var button_clear = $('button:contains("AC")');
        var button_res = $('button:contains("=")');
    
    
    
        QUnit.test( "Addition Test", function( assert ) {
          button_8.click();
          button_add.click();
          button_4.click();
          button_res.click();
          assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
          button_clear.click();
        });
    });
    
    ```

4. Run _Page_ Lab 6 so you can see that the description is _Unit Test Passed_
5. We will try to add new _Unit Test_, but we will try to make wrong_Test Case_
Put this code below into `static/js/test.js`

    ```javascript
    
    .....
        QUnit.test( "Substraction Test", function( assert ) {
          button_8.click();
          button_sub.click();
          button_4.click();
          button_res.click();
          assert.equal( $('#print').val(), 12, "8 - 4 must be 4" );
          button_clear.click();
        });
    .....
    
    ```
    
6. Run _Page_ Lab 6 so you can see that the description is _Unit Test Fail_.
    >  Now you can see the difference of failed test view and passed test view
7. Fix  the _Test Case_ so it can be _passed_
8. As a complement, add _Test_ into `static/js/test.js`
```javascript
    QUnit.test( "Multiply Test", function( assert ) {
      button_8.click();
      button_mul.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
      button_clear.click();
    });

    QUnit.test( "Division Test", function( assert ) {
      button_8.click();
      button_div.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
      button_clear.click();
    });
``` 
9. _Hide_ view from Qunit _report_ inside `lab_6/layout/base.html`
```html
.....
<div id="qunit" hidden></div>
.....
```

## Checklist

1.  Make Chat Box Page
    1. [ ] Add  _lab_6.html_ into templates folder
    2. [ ] Add _lab_6.css_ into _./static/css_
    3. [ ] Add _lab_6.js_ file into _lab_6/static/js_ folder
    4. [ ] Complete the _lab_6.js_ file so it can be run

2. Implement Calculator
    1. [ ] Add section of code into  _lab_6.html_ file on templates folder
    2. [ ] Add section of code into  _lab_6.css_ file on _./static/css_ folder
    3. [ ] Add section of code into _lab_6.js_ file on _lab_6/static/js_ folder
    4. [ ] Implement `AC` feature

3.  Implement select2
    1. [ ] Load theme default match with selectedTheme
    2. [ ] Populate data themes from local storage to select2
    3. [ ] Local storage contains themes and selectedTheme
    4. [ ] Color can change whenerver we select selectedTheme

4.  Make sure you have a good _Code Coverage_
    1. [ ]  If you have not done the configuration to show _Code Coverage_ in Gitlab, you must configure it in `Show Code Coverage in Gitlab` on [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md) 
    2. [ ] Make sure your _Code Coverage_ is 100%.

###  Challenge Checklist
1. Qunit exercise
    1. [ ] Implement from Qunit exercise
1. Chose one of these exercise you have to complete:
    1. Implement enter button on chat box
        1. [ ] Make a _Unit Test_ using Qunit
        2. [ ] Make a function that make the _Unit Test_ is _passed_ 
    1. Implement  `sin`, `log`, and `tan` functiton (HTML file is available)
        1. [ ] Make a _Unit Test_ using Qunit
        2. [ ] Maka a function that make the _Unit Test_ is _passed_